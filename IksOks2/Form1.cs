﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IksOks2
{
    public partial class Form1 : Form
    {
        bool xMove = true;

        List<Panel> panels = new List<Panel>();
        int[,] locs = new int[,] { {0,1,2 },
                                   {3,4,5 },
                                   {6,7,8 },
                                   {0,3,6 },
                                   {1,4,7 },
                                   {2,5,8 },
                                   {0,4,8 },
                                   {2,4,6 }};

        Image Iks = Image.FromFile("Iks.png");
        Image Oks = Image.FromFile("Oks.png");

        string player1Name = "";
        string player2Name = "";

        int player1Score = 0;
        int player2Score = 0;
        public Form1()
        {
            InitializeComponent();
            startFunction();
        }

        void startFunction()
        {
            panel11.BackgroundImage = Image.FromFile("Iks.png");
            panel12.BackgroundImage = Image.FromFile("Oks.png");
            panel11.BackgroundImageLayout = ImageLayout.Zoom;
            panel12.BackgroundImageLayout = ImageLayout.Zoom;

            #region Add panels
            panels.Add(panel2);
            panels.Add(panel3);
            panels.Add(panel4);
            panels.Add(panel5);
            panels.Add(panel6);
            panels.Add(panel8);
            panels.Add(panel7);
            panels.Add(panel9);
            panels.Add(panel10);
            #endregion

            disableAll();
        }
        void onClickEvent(object sender, EventArgs e)
        {
            Panel panel =(Panel)sender;
            panel.BackgroundImageLayout = ImageLayout.Zoom;
            if (xMove)
            {
                panel.BackgroundImage = Iks;
                xMove = false;
                panel.Enabled = false;
            }
            else
            {
                panel.BackgroundImage = Oks;
                xMove = true;
                panel.Enabled = false;
            }

            checkDraw();

            if(checkScore(Iks)>0)
            {
                player1Score += checkScore(Iks);
                MessageBox.Show(player1Name + " wins!");
            }
            if (checkScore(Oks) > 0)
            {
                player2Score += checkScore(Oks);
                MessageBox.Show(player2Name + " wins!");
            }
            label3.Text = player1Score.ToString();
            label4.Text = player2Score.ToString();
        }

        void handlePlayer()
        {
            if (!xMove)
            {
                label10.Text = player1Name;
            }
            else
            {
                label10.Text = player2Name;
            }
        }
        void onChangeEvent(object sender, EventArgs e)
        {
            handlePlayer();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(Panel p in panels)
            {
                p.BackgroundImage = null;
                p.Enabled = true;
            }
            xMove = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(textBox1.Text != String.Empty || textBox2.Text != String.Empty)
            {
                player1Name = textBox1.Text;
                player2Name = textBox2.Text;
                enableAll();
                panel13.BackColor = Color.Green;
                label10.Text = player1Name;
            }
            else
            {
                MessageBox.Show("Missing player names", "Warning");
            }
        }

        void enableAll()
        {
            foreach(Panel p in panels)
            {
                p.Enabled = true;
            }
        }

        void disableAll()
        {
            foreach(Panel p in panels)
            {
                p.Enabled = false;
            }
        }

        int  checkScore(Image img)
        {
            
            for(int i = 0; i<8; i++)
            {
                if(panels[locs[i,0]].BackgroundImage==img && panels[locs[i, 1]].BackgroundImage == img && panels[locs[i, 2]].BackgroundImage == img)
                {
                    return 1;
                }
            }
            return 0;
        }

        bool checkIsFull()
        {
            foreach (Panel p in panels)
            {
                if(p.BackgroundImage == null)
                {
                    return false;
                }
            }
            return true;
        }

        void checkDraw()
        {
            if(checkIsFull()&&checkScore(Iks)==0 && checkScore(Oks)==0)
            {
                MessageBox.Show("Draw game!");
            }
        }

    }
}
